# Bioflux plugin

Bioflux api wrapper service with apollo datasource graph.
Rebuild schema with suffix "BiofluxSuffix"

## Usage

### Run

set ENV

-TOKENT_BIOFLUX
-BASE_URL_BIOFLUX

run start-with-env-file

### Used for other services

copy file app/datasource/index.js
write the same query as in the resolve directory
