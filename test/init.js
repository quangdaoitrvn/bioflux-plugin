const _ = require('lodash');
const mongoose = require('mongoose');
const config = require('../app/config');

before(() => {
  // TODO: add mongoose connection
  mongoose.connect(config.database, () => {

  });
});

beforeEach(async () => {
  const collections = mongoose.Collection;
  await Promise.all(_.map(collections, (x) => x.deleteMany()));
});
