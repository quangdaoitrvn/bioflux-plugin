const container = require('./container');
const aws = require('./aws');
const db = require('./db');
const grpc = require('./grpc');
const tokens = require('./tokens');

const config = {
  ...container,
  ...aws,
  ...db,
  ...grpc,
  ...tokens,
};

module.exports = config;
