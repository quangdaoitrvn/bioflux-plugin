const ms = require('ms');

module.exports = {
  lambda: {
    region: process.env.AWS_REGION || 'us-east-2',
    chimeCreateFunction: process.env.CHIME_CREATE_FUNCTION,
    chimeJoinFunction: process.env.CHIME_JOIN_FUNCTION,
    chimeGetAttendeeFunction: process.env.CHIME_GET_ATTENDEE_FUNCTION,
    chimeEndFunction: process.env.CHIME_END_FUNCTION,
  },
  cognito: {
    identityPoolId: process.env.COGNITO_IDENTITY_POOL_ID,
    cognitoRoleArn: process.env.COGNITO_ROLE_ARN,
  },

  s3: {
    region: process.env.AWS_REGION || 'us-east-2',

    cognito: {
      identityPoolId: process.env.S3_COGNITO_IDENTITY_POOL_ID,
      cognitoRoleArn: process.env.S3_COGNITO_ROLE_ARN,
      developerProviderName: process.env.S3_COGNITO_DEVELOPER_PROVIDER_NAME,
      tokenDuration: parseInt(process.env.S3_COGNITO_TOKEN_DURATION || '20', 10),
    },

    uploadBucket: process.env.S3_UPLOAD_BUCKET,
    uploadBucketExpires: parseInt(process.env.COGNITO_UPLOAD_TOKEN_DURATION || '120', 10),

    pdfBucket: process.env.S3_REPORT_BUCKET || 'btcy-report-alpha',
    pdfExpires: parseInt(process.env.COGNITO_PDF_TOKEN_DURATION || '60', 10),
  },
};
