const custom = require('./custom');
const queryResolver = require('./queries');
const mutationResolver = require('./mutations');

module.exports = {
  Query: queryResolver,
  Mutation: mutationResolver,
  ...custom,
};
