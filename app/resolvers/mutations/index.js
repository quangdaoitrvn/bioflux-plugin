const _ = require('lodash');
const { ApolloError } = require('apollo-server-express');

async function updateStudyTypeBiofluxSuffix(parent, args, context, info) {
  const { dataSources: datasourceBioflux } = context;
  const { BiofluxAPI } = datasourceBioflux;
  const biofluxAPI = new BiofluxAPI();
  const responseData = await biofluxAPI.mutationApi(context, args, info);
  const { data, errors } = responseData;
  if (!_.isEmpty(errors)) {
    throw new ApolloError(errors[0].message);
  }
  const { updateStudyType } = data;
  return updateStudyType;
}

module.exports = {
  updateStudyTypeBiofluxSuffix,
};
