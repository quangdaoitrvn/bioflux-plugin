const { GraphQLTimestamp, GraphQLJSONObject, GraphQLDateTime, GraphQLDate } = require('graphql-scalars');

module.exports = {
  Date: GraphQLDate,
  DateTime: GraphQLDateTime,
  Timestamp: GraphQLTimestamp,
  JSONObject: GraphQLJSONObject,
};
