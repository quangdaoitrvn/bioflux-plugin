const _ = require('lodash');
const { gql } = require('apollo-server-express');
const { GraphQLDataSource } = require('apollo-datasource-graphql');

const suffix = 'BiofluxSuffix';
class GraphqlBiofluxAPI extends GraphQLDataSource {
  baseURL = process.env.BASE_URL_BIOFLUX;

  willSendRequest = request => {
    if (!request.headers) {
      request.headers = {};
    }
    request.headers = {
      'access-token': process.env.TOKENT_BIOFLUX,
      'Content-Type': 'application/json',
    };
  }

  async queryApi(context, args, info) {
    const { gqlQuery, variables } = createQuery(info);
    const response = await this.query(gqlQuery, variables);
    return response;
  }

  async mutationApi(context, args, info) {
    const { gqlQuery, variables } = createQuery(info);
    const response = await this.mutation(gqlQuery, { variables });
    return response;
  }
}

function createQuery(info) {
  const queryString = info.fieldNodes[0].loc.source.body;
  const { variableValues: variables } = info;
  const queryReplace = _.replace(queryString, new RegExp(suffix, 'g'), '');
  const gqlQuery = gql`${queryReplace}`;
  return { gqlQuery, variables };
}

module.exports = () => ({ BiofluxAPI: GraphqlBiofluxAPI });
